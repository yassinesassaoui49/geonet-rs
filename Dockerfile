# Prebuilt image for Rust cross-compiling, by default we only able to build host (rust:latest image architecture: Linux) birary.
FROM rust:latest

RUN apt-get update -y && apt-get install cmake llvm-dev libclang-dev clang libxml2-dev libz-dev gcc-mingw-w64-x86-64 -y

ENV MACOSX_CROSS_COMPILER=/macosx-cross-compiler
RUN mkdir -p $MACOSX_CROSS_COMPILER/cross-compiler

# Clone toolchain build scripts
WORKDIR $MACOSX_CROSS_COMPILER
RUN git clone https://github.com/tpoechtrager/osxcross

# Download prebuilt MacOSX SDK
RUN wget https://github.com/phracker/MacOSX-SDKs/releases/download/11.3/MacOSX11.3.sdk.tar.xz -P osxcross/tarballs

# Build MacOS toolchain
RUN UNATTENDED=yes OSX_VERSION_MIN=10.15.7 TARGET_DIR=$MACOSX_CROSS_COMPILER/cross-compiler ./osxcross/build.sh
ENV PATH=$MACOSX_CROSS_COMPILER/cross-compiler/bin:$PATH
COPY cargo-config.toml /usr/local/cargo/config

# Add additional x64 targets
RUN rustup target add x86_64-apple-darwin aarch64-apple-darwin x86_64-pc-windows-gnu